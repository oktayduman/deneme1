import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class Main {

	static int N = 9;
	static int dizi[][];
	static int costDizi[][];
	static int sayac = 0;
	static long c = 0;
	static Random random;
	static long f,l;
	
	static List<Hucre> hucreList;
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// System.out.println("Bismillah ");

		dizi = new int[N][N];
		costDizi = new int[N][N];
		hucreList = new ArrayList<>();
		random = new Random();
		f=Calendar.getInstance().getTimeInMillis();
		start();
		
		l=Calendar.getInstance().getTimeInMillis();
		long fark = l-f;
		System.out.println("fark =  "+fark);

	}

	public static void start() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				dizi[i][j] = 0;
			}

		}
		
		/*
		 * 
		 * diziye başlangıç değerlerini ata 
		 * */

		String diziString =
				  "7 5 3 1 2 0 0 6 8,"
				+ "4 8 0 6 5 0 0 0 0,"
				+ "2 6 1 0 8 0 0 0 0,"
				+ "0 1 0 0 3 0 0 0 0,"
				+ "8 0 0 0 7 0 0 0 0,"
				+ "3 0 0 0 4 0 0 0 0,"
				+ "0 3 2 0 1 0 0 0 0,"
				+ "0 0 0 0 6 5 0 9 7,"
				+ "5 7 0 0 9 2 0 1 3,"
				;
		
		String diziList[] = diziString.split(",");
		// sonra onlara inte çöyür
		for(int i=0;i<diziList.length;i++){
			//System.out.println("  "+diziList[i]);
			
			String tempArray[] = diziList[i].split(" ");
			for(int j=0;j<tempArray.length;j++){
				//System.out.println(" "+tempArray[j]);
				dizi[i][j]=Integer.parseInt(tempArray[j]);
			}
		}
		
		
		
		//bi liste ata 
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				
				if(dizi[i][j]==0){
					costDizi[i][j]=sayac(i,j);// her hücrenin costunu maliyetini hespla
					Hucre hucre = new Hucre(i, j, sayac(i,j));
					hucreList.add(hucre);
				}
				
			}
		}
				
		
		
		for(Hucre h:hucreList){
			System.out.println(" asss "+h.getCost());
		}
		// sonra büyükten küçüğe sırala
		for (int i = 0; i < hucreList.size(); i++) {
			for (int j = 0; j <hucreList.size()-1; j++) {
				if(hucreList.get(j).getCost()< hucreList.get(j+1).getCost()){
					Hucre temp = hucreList.get(j);
					hucreList.set(j, hucreList.get(j+1));
					hucreList.set(j+1, temp);
				}
			}
			
		}
		
		
		for(Hucre h:hucreList){
			System.out.println(" c "+h.getCost() +"  "+h.getX()+ "  "+h.getY());
		}
		
		yazdir();

		
		System.out.println("");
		System.out.println("");
		System.out.println("");
		
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				System.out.print("  " + costDizi[i][j]);
			}
			System.out.println("");

		}
		

		for (int i = 0; i <= N; i++) {
			//ilerle(i);
		}

	}
	
	
	public static int sayac(int x,int y){
		int k=0;
		for (int i = 0; i < N; i++) {
			if(dizi[x][i]!=0){
				k++;
			}
			
		}
		
		
		for (int i = 0; i < N; i++) {
			if(dizi[i][y]!=0){
				k++;
			}
			
		}
		return k;
	}

	public static void yazdir() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				System.out.print("  " + dizi[i][j]);
			}
			System.out.println("");

		}
	}

	public static boolean ilerle(int y) {

		for (int i = 0; i < y; i++) {
			for (int j = 0; j < y;) {
				if (dizi[i][j] == 0) {
					int r = random.nextInt(N) + 1;
					if (isLegal(i, j, r)) {
						dizi[i][j] = r;
						// cizz();
						sayac = 0;

						System.out.println("***");
						System.out.println("***");
						System.out.println("***");
						System.out.println(" x" + i + " y = " + j + " z = " + r
								+ " sayac = " + sayac + "c   " + c);
						yazdir();
						j++;
					} else {
						continue;
					}
				} else {
					j++;
					continue;
				}
			}
		}
		return true;

	}

	public static boolean isLegal(int x, int y, int z) {

		int t = 0;
		sayac++;
		c++;
		// System.out.println();

		for (int i = 0; i < N; i++) {
			if (z == dizi[x][i] || z == dizi[i][y]) {
				t++;
			}
		}
		// System.out.println(" x"+x+" y = "+y+" z = "+z+" sayac = "+sayac+
		// "c   "+c);
		if (sayac > 1000) {
			sayac = 0;
			System.out.println("                             bulamadı ");
			start();
		}

		int r = (int) Math.sqrt(N);

		int i1 = (x / r) * r;
		int j1 = (y / r) * r;
		for (int i = i1; i < i1 + r; i++) {
			for (int j = j1; j < j1 + r; j++) {
				if (dizi[i][j] == z) {
					t++;
				}
			}
		}

		if (t == 0) {
			return true;
		} else {
			return false;
		}

	}

}
